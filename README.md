

# Usage
To get a key from storage:
```lua
world_storage.get(domain, key)
```

To save a key to storage:
```lua
world_storage.set(domain, key, val, flags)
```


# As a world settings solution
You can also use the chat command to set custom settings that can be used by your mods instead of the global minetest server settings. Note this cannot affect domains other than "_ws", and this is its intended and only valid use. The first param is `n` for number or `s` for string / raw. If it is not of the same type, it will reject your attempt to change it, to prevent this from being used to crash a server.
```
wset n key 389.2
wset s key some string of letters, even with spaces
wset [anything] key nil -- deletes a key
```
Then, these values can be accessed with wset and wget.
```lua
local value = world_storage.wget("my_mod:setting_name")
-- e.g.
local reload_time = world_storage.wget("pmb_muskets:reload_speed")
```
To make sure you don't get nil or have to check for it all the time, you can use `world_storage.wcheck` with all of your stored variables. This will only set the var if it was nil, so you don't have to make your own logic for it. For this, you may want to put this in an `on_mods_loaded` callback.
```lua
-- world_storage.wcheck(setting_name, default_value)
world_storage.wcheck("pmb_muskets:reload_speed", 1)
```



# Parameters
## domain
This is the `filename` used for the saving of the data. Each domain is a list - `world_storage.domain[domainname] = {}`.
You can overwrite keys from other mods with this, so choose a unique name, such as the name of your mod.
## key
The key name / variable name you want to save
## val
The arbitrary data you want to save
## flags
Only one flag implemented so far:
- timeout : used to tell the system how long between autosaves



# Notes on usage
The system will autosave regularly if there are changes made, and this is important to note. If you are saving a lot of data, this is not the right system to use. Instead, construct your own system to handle your specific use case. For example, if you need to store several kilobytes of data regularly, **this is not how you do it.**

